# bounded-qsem

Bounded version of the quantity semaphores provided by
[base](https://hackage.haskell.org/package/base/docs/Control-Concurrent-QSem.html).